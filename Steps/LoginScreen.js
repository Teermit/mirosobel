const { BeforeAll, Before, Given, When, Then , After, AfterAll } = require('cucumber')
const assert = require('assert')
const {setup} = require('../utils/connector')

let driver;

BeforeAll({timeout: 30*1000}, async function() {
    console.log('Connecting to device')
    driver = await setup()
})

After(async function() {
    console.log('Resetting app')
    await driver.resetApp()
})

Given(/^User is on login screen$/, async function () {
    //Действий не требуется, юзер по дефолту на экране логина
})

When(/^Login is \"(.*)\"$/, async function(login) {
    var textElements = await driver.elementsByClassName("android.widget.EditText")
    var loginElement = textElements[0]
    await loginElement.clear()
    await loginElement.sendKeys(login)
    assert.equal(await loginElement.text(), login)
})

When(/^Password is \"(.*)\"$/, async function(password) {
    var textElements = await driver.elementsByClassName("android.widget.EditText")
    var passwordElement = textElements[1]
    await passwordElement.clear()
    await passwordElement.sendKeys(password)
    assert.equal(await passwordElement.text(), '•'.repeat(password.length))
})

When(/^Try login$/, async function() {
    var loginButton = await driver.elementByXPath("//*[@text='Sign In']/..")
    await loginButton.click()
})

Then(/^Login fails$/, async function() {
    var a = await driver.elementById("android:id/alertTitle")
    assert.equal(await a.text(), 'Invalid username or password')
})

Then(/^Login ok$/, async function() {
    var a = await driver.elementByXPath("//*[@text='All boards']")
    assert.notEqual(a, null)
})

When(/^Google login button pressed$/, async function() {
    //XPath вида [last() - 5] не работает здесь
    var buttons = await driver.elementsByXPath("//android.widget.ImageView/..")
    var googleButton = buttons[buttons.length - 5]
    googleButton.click()
})

Then(/^Google account picker opens$/, async function() {
    var container = await driver.elementById("com.google.android.gms:id/account_picker_container")
    assert.notEqual(container, null)
})

AfterAll(async function() {
    console.log('Disconnecting...')
    await driver.quit()
})