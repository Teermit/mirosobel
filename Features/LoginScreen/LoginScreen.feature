Feature: LoginScreen

Background:
Given User is on login screen

Scenario: Invalid login 1
When Login is "owl"
And Password is "uu"
And Try login
Then Login fails

Scenario: Invalid login 2
When Login is "'DROP TABLE Users--"
And Password is "https://xkcd.com/327/"
And Try login
Then Login fails

Scenario: Correct login
When Login is "terranova5@yandex.ru"
And Password is "aeL9eim4oh"
And Try login
Then Login ok

Scenario: Google login
When Google login button pressed
Then Google account picker opens