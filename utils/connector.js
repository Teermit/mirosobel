const fs = require('fs')
const path = require('path')
const wd = require('wd')
const {setDefaultTimeout} = require('cucumber');

exports.setup = async function() {
    var settingsContent = fs.readFileSync("settings.json", "utf8")
    var settings = JSON.parse(settingsContent)
    settings.caps.app = path.resolve(settings.caps.app)
    var driver = wd.promiseChainRemote(settings.server)
    await driver.init(settings.caps)
    await driver.setImplicitWaitTimeout(3600)
    setDefaultTimeout(3000*1000)
    return driver
}